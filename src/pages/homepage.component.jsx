import React from 'react';
import './homepage.styles.scss';
import MenuItem from '../components/menu-item/menu-item.component';

const HomePage = () => (
    <div className="homepage">
        <MenuItem/>
    </div>

);
    
export default HomePage;