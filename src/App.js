import React from 'react';
import HomePage from './pages/homepage.component';
import MenuItem from './components/menu-item/menu-item.component';
import Directory from './components/directory/directory.component';

function App() {
  return (
    <div className="App">
      <HomePage/>
    </div>
  );
}

export default App;
